﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Patron_Singleton
{
    class Singleton
    {
        // El constructor de Singleton siempre debe ser privado para evitar
        // llamadas de construcción directas con el operador `new`.
        private Singleton() { }

        // La instancia de Singleton se almacena en un campo estático. Existen
        // múltiples formas de inicializar este campo, todas tienen varias ventajas
        // y contras. En este ejemplo, mostraremos la más simple de estas formas,
        // que, sin embargo, no funciona muy bien en programas multiproceso.
        private static Singleton _instance;

        // Este es el método estático que controla el acceso al singleton
        // instancia. En la primera ejecución, crea un objeto singleton y coloca
        // en el campo estático. En ejecuciones posteriores, devuelve el cliente
        // objeto existente almacenado en el campo estático.
        public static Singleton GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Singleton();
            }
            return _instance;
        }

        // Finalmente, cualquier singleton debe definir alguna lógica de negocios, que puede
        // se ejecutará en su instancia.
        public static void someBusinessLogic()
        {
            // ...
        }
    }
}
